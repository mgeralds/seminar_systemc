// Code your testbench here.
// Uncomment the next line for SystemC modules.
#include "systemc.h"
#include "design.cpp"

int sc_main (int argc, char* argv[]) {
  int i =0;
  sc_signal<bool>   clk;
  sc_signal<bool>   reset;
  sc_signal<bool> inc;
  sc_signal<bool> load;
  sc_signal<sc_uint<12>> imm;
  sc_signal<sc_uint<12>> pc;
  

  // Connect the DUT
  counter count("COUNTER");
  count.clk(clk);
  count.reset(reset);
  count.inc(inc);
  count.load(load);
  count.imm(imm);
  count.pc(pc);
  
  //go on 1 ns 
  sc_start(1, SC_NS);

  // Open VCD file
  sc_trace_file *wf = sc_create_vcd_trace_file("count");
  // Dump the desired signals
  sc_trace(wf, clk, "clk");
  sc_trace(wf, reset, "reset");
  sc_trace(wf, inc, "inc");
  sc_trace(wf, load, "load");
  sc_trace(wf, imm, "imm");
  sc_trace(wf, pc, "pc");
  
  //Assert reset
  reset=1;
  cout << "@" << sc_time_stamp() <<" asserting reset\n" << endl;
  
 
  //go on 4 ns - 2 clk cycle
    for (i=0;i<2;i++) {
    clk = 0; 
    sc_start(1, SC_NS);
    clk = 1; 
    sc_start(1, SC_NS);
  }
  //Deassert reset 
  reset = 0;
  cout << "@" << sc_time_stamp() <<" Deasserting reset\n" << endl;
  
  //go on 4 ns - 2 clk cycle
for (i=0;i<2;i++) {
    clk = 0; 
    sc_start(1, SC_NS);
    clk = 1; 
    sc_start(1, SC_NS);
  }
  
  // load inputs
  inc = 1;
  load = 0;
  imm = 17;
  
  //go on 10 ns - 5 clk cycle
  for (i=0;i<5;i++) {
    clk = 0; 
    sc_start(1, SC_NS);
    clk = 1; 
    sc_start(1, SC_NS);
  }
  
  //Assert load
  load = 1;
  
  //go on 2 ns - 1 clk cycle
  for (i=0;i<1;i++) {
    clk = 0; 
    sc_start(1, SC_NS);
    clk = 1; 
    sc_start(1, SC_NS);
  }
  
  //Deassert load
  load =0;
  
  //go on 10 ns - 5 clk cycle
  for (i=0;i<5;i++) {
    clk = 0; 
    sc_start(1, SC_NS);
    clk = 1; 
    sc_start(1, SC_NS);
  }
  
  //Assert reset
  reset = 1;
  cout << "@" << sc_time_stamp() <<" asserting reset\n" << endl;
  
  //go on 2 ns - 1 clk cycle
  for (i=0;i<1;i++) {
    clk = 0; 
    sc_start(1, SC_NS);
    clk = 1; 
    sc_start(1, SC_NS);
  }
  
  //Deassert reset
  reset = 0;
  cout << "@" << sc_time_stamp() <<" Deasserting reset\n" << endl;
  
  //go on 10 ns - 5 clk cycle
  for (i=0;i<5;i++) {
    clk = 0; 
    sc_start(1, SC_NS);
    clk = 1; 
    sc_start(1, SC_NS);
  }
  
  
  
  //Simulation ends here
  cout << "@" << sc_time_stamp() <<" Terminating simulation\n" << endl;
  
  //close trace file
  sc_close_vcd_trace_file(wf);
  return 0;// Terminate simulation
}
