#include "systemc.h"

SC_MODULE (counter) {
 //PORTS
sc_in_clk clk;
sc_in<bool> reset;
sc_in<bool> inc;
sc_in<bool> load;
sc_in<sc_uint<12>> imm;
sc_out<sc_uint<12>> pc;

//local variable
sc_uint<12> pc_next;
sc_uint<12> pc_temp;

//Function imm_next(): determine the incremented value of pc (pc_next is the pending value to be asserted to pc, if clk = positive edge, reset =0, inc = 1, load = 0)
void imm_next()
{
  
  pc_next = pc_temp+1;
}

//Function pc_func(): determine the value of output "pc" through the variable "pc_temp" (if reset = 1, pc =0, if reset = 0 and inc = 1 and load = 1 then pc = imm, if reset = 0 and inc = 1 and load = 0 then pc = pc_next, if reset=0 and inc =0 then the value of pc stays
void pc_func()
{
 
  if (reset.read()==1)
  {
    pc_temp = 0;
   
  }
  else
  {
     if (inc.read()==1)
     {
       if (load.read()==1)
       { 
         pc_temp = imm.read();
       }
       else 
       {
         pc_temp = pc_next;
       }
     }
    else
    {
      pc_temp = pc_temp;
    }
  }
  
  //Write value of the variable "pc_temp" to the port "pc"
  pc.write(pc_temp);
  //Notify user the pc anc pc_next on the current time stamp
  cout<< sc_time_stamp() << " | pc = "<< pc.read() << " | pc_next = "<< pc_next << endl;
  
}

  //Constructor of the module (What the object/module would look like as an instance in testbench/other parts
  SC_CTOR(counter) {
  //Notify the user that the simulation starts (a new program counter is made)
  cout<<"Executing new"<<endl;
    
    //Method imm_next with the sensitivity list of (clk'event and clk = 1) and reset
    SC_METHOD(imm_next);
    sensitive << clk.pos();
    sensitive << reset;
    //Method pc_func with the sensitivity list of (clk'event and clk = 1) and reset
    SC_METHOD(pc_func);
    sensitive << reset;
    sensitive << clk.pos();
    
    //Be wary that processes pc_func and imm_next are equivalent to processes with sensitivity lists in, let's say, VHDL
  }
  
};