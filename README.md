# SEMINAR_SYSTEMC

This is an example SystemC code which will be used as an example for the term paper



## How to Run The Source Code

### First Method: EDA Playground

1. Open https://www.edaplayground.com/
2. Make sure that the settings on the tool bar on the left side are set to the following: 
   1. Testbench + Design = C++/SystemC
   2. Libraries = SystemC 2.3.3 selected
   3. Toold & Simulators = C++
   4. Check the checkbox for "Open EPWave after run"
   5. Keep all other options empty
3. Copy and paste the code (design.cpp and testbench.cpp) to the appropriate window
4. Click save on the top left  side of the website
5. Click run (top left side of the website)
6. You'll notice there is a Popup. That is the wave diagram from the trace file (trace.vcd) from the EPWave (Remember: you have checked the option). Click the "_" icon on the top right to minimize
7. You may see the logs of the simulation on the "Log" part of the website (bottom side)
8. If you want to modify the code, go ahead. To simulate, repeat the steps 4 to 7.

### Second Method: Visual Studio

Refer to the [following video](https://www.youtube.com/watch?v=0VxvIzVdoaI). This currently works on Visual Studio 2019 Community Edition. Graphical analysis of the value change dump data [can be done in GTKWave in Windows](https://youtu.be/H2GyAIYwZbw?t=1867)

### Third Method: g++ (in Linux)

This might be the most complex way to simulate SystemC designs. Though, this might be very handy to see the compiler directly in action. These steps are intended for SystemC 2.3.3 and Linux based operating systems. (References: https://www.electrobucket.com/systemc/getting-started-with-systemc/ and https://www.youtube.com/watch?v=YvzS5ENCbdk with some modifications)

0. Before proceeding, you must install the g++ compiler 

   `sudo apt-get update`

   `sudo apt install g++`

1. Install [the tar.gz SystemC Language and Examples here](https://www.accellera.org/downloads/standards/systemc)

2. Untar the tar.gz:

   `*tar -xzvf systemc-2.3.3.tar.gz*`

3. Change to the top level directory systemc-2.3.3

   `cd systemc-2.3.3`

4. Make a directory for SystemC: 

   ```
   sudo mkdir /usr/local/systemc233
   ```

5. Create a directory for object code using 

   `mkdir objdir`

6. Change directory to objdir using 

   `cd objdir`

7. Run configure from your top level directory: 

   `../configure --prefix=/usr/local/systemc233`

8. Run 

   `make`

9. Run 

   `sudo make install`

10. Run the following to compile design.cpp and testbench.cpp. Make sure that design.cpp and testbench.cpp are in your working directory!

    `g++ -I. -I/usr/local/systemc233/include -L. -L/usr/local/systemc233/lib-linux64 -o test testbench.cpp design.cpp -lsystemc -lm`

    `-I. -I/usr/local/systemc233/include` denotes the search directory for header files

    `-L. -L/usr/local/systemc233/lib-linux64` denotes the search directory for library files

    `-o test` denotes the output file for the compiled code

    `testbench.cpp design.cpp` are the source codes that are to be compiled

    `-lsystemc`search libarary named systemc

    `-lm` load move(?)

11. Execute the compiled source code. You should see the logs in the terminal

    `./test`

12. You may see a `trace.vcd` file in your working directory. That is the trace file (value change dump) that denotes the changes of values in signals/ports of the simulation based on a timely basis. It can be opened by [GTKWave](http://gtkwave.sourceforge.net/) (in Linux - [manual](https://youtu.be/dvLeDNbXfFw?t=1307))